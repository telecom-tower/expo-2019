package main

import (
	"context"
	"flag"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/telecom-tower/sdk"
	"golang.org/x/image/colornames"
	"google.golang.org/grpc"
)

func check(err error, msg string) {
	if err != nil {
		err = errors.WithMessage(err, msg)
		log.Fatal(err)
	}
}

func main() {
	debug := flag.Bool("debug", false, "Run in debug mode")
	grpcUrl := flag.String("url", "localhost:10000", "grpc URL")
	flag.Parse()

	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.WarnLevel)
	}

	log.Infof("Connecting to tower server : %v", *grpcUrl)
	conn, err := grpc.Dial(*grpcUrl, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Error dialing server: %v", err)
	}
	defer conn.Close() // nolint: errcheck
	client := sdk.NewClient(conn)

	msg := `<text><font color="#FFFFFF">Ton avenir commence ici<font color="#00BFFF"> &gt;&gt;&gt; </font>Deine Zukunft beginnt hier</font><font color="#00BFFF"> &gt;&gt;&gt; </font></text>`
	check(client.StartDrawing(context.Background()), "Error getting frame")
	check(client.Init(), "Error initializing display")
	check(client.WriteText(msg, "6x8", 0, colornames.White, 0, sdk.PaintMode), "Error writing text")
	check(client.AutoRoll(0, sdk.RollingNext, 0, 0), "Error setting autoroll")
	check(client.Render(), "Error rendering")

	log.Debug("Done")
}
