#!/bin/bash

APP="expo-2019"

mkdir -p dist
for armv in 5 6 7; do
    env GOOS=linux GOARCH=arm GOARM=$armv go build -o dist/${APP}-armv$armv
done
